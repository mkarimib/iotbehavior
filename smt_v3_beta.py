#!/usr/bin/python

"""
Parse and check policy files
for safety check of an IoT system
"""
from z3 import *
import xml.etree.ElementTree as ET
import os

# Set up option parsing for smt run
import argparse

# list of attributes and elements for each policy
# is saved in to the list
rulelist=[]
ruleatt=[]
dtype=[]
vendor=[]
daysAvailableInWeek=[]
timeatr=[]
user=[]
group=[]

class bcolors:
    '''
        notifciation colors
    '''
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def parse(xmlFile):
    """
        Parse with element tree library
    """
    tree = ET.parse(xmlFile)
    root = tree.getroot()

    for rule in root.iter('rule'):
        ruleatt.append(rule.attrib)
        rulelist.append(rule.text)

    for dtype2 in root.iter('type'):
        dtype.append(dtype2.text)

    for vendor2 in root.iter('vendor'):
        vendor.append(vendor2.text)

    for time2 in root.iter('time'):
        timeatr.append(time2.attrib)
        daysAvailableInWeek.append(time2.text)

    for user2 in root.iter('user'):
        user.append(user2.text)

    for group2 in root.iter('group'):
        group.append(group2.text)

def printtree():
    '''
        print for demo
    '''
    print ("rulelist=")
    pp(rulelist)
    print("-----------------")
    print ("ruleatt=")
    pp(ruleatt)
    print("-----------------")
    print ("dtype=")
    pp(dtype)
    print("-----------------")
    print ("vendor=")
    pp(vendor)
    print("-----------------")
    print ("daysAvailableInWeek=")
    pp(daysAvailableInWeek)
    print("-----------------")
    print ("timeatr=")
    pp(timeatr)
    print("-----------------")
    print ("user=")
    pp(user)
    print("-----------------")
    print ("group=")
    pp(group)
    print("-----------------")

def smt():
    '''
        The SMT solver function using Z3
    '''

    ###############################
    ### Helper functions secton ###
    ###############################
    def getVendorId(vendorName):
        """
            Helper inner function
            return and ID integer value
            for now just a number 
        """
        if vendorName.lower() == "room1":
            return 1
        elif vendorName.lower() == "room2":
            return 2
        elif vendorName.lower() == "room3":
            return 3
        else:
            return None

    def daysXint(stringDay):
        ''' 
            Helper inner function
            pass a string and return a 
            set of integers earch representing a day
        ''' 
        daySet = set()
        if "Mon" in stringDay:
            daySet.add(1)

        if "Tue" in stringDay:
            daySet.add(2)

        if "Wed" in stringDay:
            daySet.add(3)

        if "Thu" in stringDay:
            daySet.add(4)

        if "Fri" in stringDay:
            daySet.add(5)

        if "Sat" in stringDay:
            daySet.add(6)

        if "Sun" in stringDay:
            daySet.add(7)

        return daySet

    def timeXseconds(time_str):
        '''
            Helper inner function 
            return the list for min max as seconds
            between zero and 24*3600 s
            TODO: use list comprehension in future
        ''' 
        def get_sec(time_str):
            h, m = time_str.split(':')
            return int(h) * 3600 + int(m) * 60

        return get_sec(time_str)
# ###########################################################
    # #################################################
    # ### Start iterating through the policy rules ####
    # #################################################
    
    # Symbolic execution: create a new Z3 solver
    solver = Solver() 

    # symbolic time
    _symb_time = Real('timeOfDay')
    solver.add(_symb_time>=0)
    solver.add(_symb_time<=24*3600) # time of the day in seconds
    
    _symb_dayOfWeek = Int('dayOfWeek')
    solver.add(_symb_dayOfWeek>=1) # 1 stands for Monday
    solver.add(_symb_dayOfWeek<=7) # 7 stands for Sunday

    # >>>>>>>> Create all symbolic variables <<<< 
    # how many distinct sensors in vendors
    # 2 rooms, 3 sensors in every room -> 6 symbols
    # Turn into set: sets have no order and no repeated element
    dtypeSet = set(dtype)
    vendorSet = set (vendor)

    _symb_dtype = RealVector('_symb_dtype',\
                              len(dtypeSet)*len(vendorSet))

    # TO FIX: set does not keep the order!
    dtypeSetList = list(dtypeSet)
    vendorSetList = list(vendorSet)

    smtVarsDict = dict()
    count = 0
    for i in range(len(vendorSet)):
        for j in range(len(dtypeSet)):
            smtVarsDict.update({vendorSetList[i]+dtypeSetList[j]:\
                           _symb_dtype[count]})
            count = count + 1

    # pp(smtVarsDict)

    # Removed below, b/c these are hard rules
    # _symb_temp = Real('temperature')
    # solver.add(_symb_temp>=-20)
    # solver.add(_symb_temp<=80)
    
    satisfiedRules=[]
    satisfiedPolicies=0
    
    # Symbolic vendor id like room1, airport, drone, etc.
    _symb_vendor=Int('vendor')

    for i in range(len(rulelist)):
        
        # Check for a particular vendor
        ruleIsSatisfied = _symb_vendor==getVendorId(vendor[i])

        # Variable range Check
        # ruleIsSatisfied = ruleIsSatisfied and \
        #         smtVarsDict[vendor[i]+dtype[i]] >= ruleatt[i]['min']\
                           # and \
        #         smtVarsDict[vendor[i]+dtype[i]] <= ruleatt[i]['max']
        solver.add(smtVarsDict[vendor[i]+dtype[i]] >= ruleatt[i]['min'])
        solver.add(smtVarsDict[vendor[i]+dtype[i]] <= ruleatt[i]['max']) 

        # # Day of the week in a set
        # dayIsAcceptable = False
        # iterateSet = daysXint(daysAvailableInWeek[i])
        # for d in iterateSet:
        #     dayIsAcceptable = dayIsAcceptable or _symb_dayOfWeek == d

        # # Time range
        # ruleIsSatisfied = ruleIsSatisfied and dayIsAcceptable and\
        #  _symb_time >= timeXseconds(timeatr[i]['min']) and \
        #  _symb_time <= timeXseconds(timeatr[i]['max'])

        satisfiedRules.append(ruleIsSatisfied)

        satisfiedPolicies= If( ruleIsSatisfied,
                               satisfiedPolicies+1,
                               satisfiedPolicies)
    
    solver.add(satisfiedPolicies > 1)

    if solver.check() == sat:
        print bcolors.FAIL+bcolors.UNDERLINE+\
              u'\u274c'+" found a safety issue"+\
              bcolors.ENDC

        model = solver.model()
        # print("temperature is "+ str(model.evaluate(_symb_temp)))
        # print("temperature is "+ str(model.evaluate(_symb_dtype)))
        # print("time is "+ str(model.evaluate(_symb_time)))
        # print("room is "+ str(model.evaluate(_symb_vendor)))

        for i,policy in enumerate(satisfiedRules):
            if model.evaluate(policy):
                print "one of the confliciting is" +\
                      " rule#" + str(i) + " " +  str(policy)


    else:
        print bcolors.OKGREEN+\
              u'\u2714'+" all policies are good"+\
              bcolors.ENDC

def main(xmlFile):
    '''     
        The main function
    '''
    print("parse xml file...")
    parse(xmlFile)
    
    if printFlag:
        print("\n---printing---")
        printtree()

    if smtFlag:
        print("\nsmt...")
        smt()  

# ########################
# ######## MAIN ##########
# ########################
if __name__ == '__main__':
    
    # Set options
    parser = argparse.ArgumentParser(description=bcolors.HEADER+\
                                    'Symbolic execution for policy' +\
                                    bcolors.ENDC)
    parser.add_argument( '--smt',
                         type=int,
                         default=0,
                         help="smt flag, i.e., --smt 1")
    parser.add_argument( '--file',
                         default='sample.xml',
                         help="pass the xml file name\
                          , e.g. \
                          --file sample.xml")
    parser.add_argument('-p', action="store_true",\
                             default=False,\
                             help='print option to\
                              parse policy file')
    args = parser.parse_args()
    smtFlag = args.smt
    printFlag = args.p
    # must be in the same directory
    xmlFile = args.file

    name_of_policy_folder = "policyFiles"
    file_path = os.path.join(name_of_policy_folder,xmlFile)

    # Call the main function
    main(file_path)