#!/usr/bin/python

"""
Parse and check policy files
for safety check of an IoT system
"""
from z3 import *
import xml.etree.ElementTree as ET
import os

# Set up option parsing for smt run
import argparse

# list of attributes and elements for each policy
# is saved in to the list
rulelist=[]
ruleatt=[]
dtype=[]
vendor=[]
daysAvailableInWeek=[]
timeatr=[]
user=[]
group=[]

class bcolors:
    '''
        notifciation colors
    '''
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def parse(xmlFile):
    """
        Parse with element tree library
    """
    tree = ET.parse(xmlFile)
    root = tree.getroot()

    for rule in root.iter('rule'):
        ruleatt.append(rule.attrib)
        rulelist.append(rule.text)

    for dtype2 in root.iter('type'):
        dtype.append(dtype2.text)

    for vendor2 in root.iter('vendor'):
        vendor.append(vendor2.text)

    for time2 in root.iter('time'):
        timeatr.append(time2.attrib)
        daysAvailableInWeek.append(time2.text)

    for user2 in root.iter('user'):
        user.append(user2.text)

    for group2 in root.iter('group'):
        group.append(group2.text)

def printtree():
    '''
        print for demo
    '''
    print ("rulelist=")
    pp(rulelist)
    print("-----------------")
    print ("ruleatt=")
    pp(ruleatt)
    print("-----------------")
    print ("dtype=")
    pp(dtype)
    print("-----------------")
    print ("vendor=")
    pp(vendor)
    print("-----------------")
    print ("daysAvailableInWeek=")
    pp(daysAvailableInWeek)
    print("-----------------")
    print ("timeatr=")
    pp(timeatr)
    print("-----------------")
    print ("user=")
    pp(user)
    print("-----------------")
    print ("group=")
    pp(group)
    print("-----------------")

def smt():
    '''
        The SMT solver function using Z3
    '''

    ###############################
    ### Helper functions secton ###
    ###############################
    def getRoomId(roomName):
        """
            Helper inner function
            return and ID integer value
            for now just a number 
        """
        if roomName.lower() == "room1":
            return 1
        elif roomName.lower() == "room2":
            return 2
        elif roomName.lower() == "room3":
            return 3
        else:
            return None

    def daysXint(stringDay):
        ''' 
            Helper inner function
            pass a string and return a 
            list of integers
            indicating the day
        ''' 
        global dayList
        dayList = 0x00
        if "Mon" in stringDay:
            dayList = dayList | 0x01

        if "Tue" in stringDay:
            dayList = dayList | 0x02

        if "Wed" in stringDay:
            dayList = dayList | 0x04

        if "Thu" in stringDay:
            dayList = dayList | 0x08

        if "Fri" in stringDay:
            dayList = dayList | 0x10

        if "Sat" in stringDay:
            dayList = dayList | 0x20

        if "Sun" in stringDay:
            dayList = dayList | 0x40

        return dayList

    def timeXseconds(time_str):
        '''
            Helper inner function 
            return the list for min max as seconds
            between zero and 24*3600 s
            TODO: use list comprehension in future
        ''' 
        def get_sec(time_str):
            h, m = time_str.split(':')
            return int(h) * 3600 + int(m) * 60

        return get_sec(time_str)
# ###########################################################
    # #################################################
    # ### Start iterating through the policy rules ####
    # #################################################
    
    # Symbolic execution: create a new Z3 solver
    solver = Solver() 

    # symbolic time
    _symb_time = Real('timeOfDay')
    solver.add(_symb_time>=0)
    solver.add(_symb_time<=24*3600) # time of the day in seconds
    
    _symb_dayOfWeek = Int('dayOfWeek')
    solver.add(_symb_dayOfWeek>=0) # zero stands for Monday
    solver.add(_symb_dayOfWeek<7) # 6 stands for Sunday

    # >>>>>>>> TODO: it could be humidity? then what? <<<<
    _symb_temp = Real('temperature')
    solver.add(_symb_temp>=-20)
    solver.add(_symb_temp<=80)
    
    satisfiedRules=[]
    satisfiedPolicies=0
    
    # >>>>>> TODO: it could be hallway, then what? <<<<
    _symb_room=Int('roomID')

    for i in range(len(rulelist)):
        
        ruleIsSatisfied = _symb_room==getRoomId(vendor[i])
        
        # Variable range
        ruleIsSatisfied = ruleIsSatisfied and \
                _symb_temp >= ruleatt[i]['min'] and \
                _symb_temp <= ruleatt[i]['max']

        # Day of the week
        dayIsAcceptable = False
        dayIsAcceptable = dayIsAcceptable or \
            (_symb_dayOfWeek == daysXint(daysAvailableInWeek[i]))

        # Time range
        ruleIsSatisfied = ruleIsSatisfied and dayIsAcceptable and\
         _symb_time >= timeXseconds(timeatr[i]['min']) and \
         _symb_time <= timeXseconds(timeatr[i]['max'])

        satisfiedRules.append(ruleIsSatisfied)

        satisfiedPolicies= If( ruleIsSatisfied,
                               satisfiedPolicies+1,
                               satisfiedPolicies)
    
    solver.add(satisfiedPolicies > 1)

    if solver.check() == sat:
        print bcolors.FAIL+bcolors.UNDERLINE+\
              u'\u274c'+" found a safety issue"+\
              bcolors.ENDC  
    else:
        print bcolors.OKGREEN+\
              u'\u2714'+" all policies are good"+\
              bcolors.ENDC

def main(xmlFile):
    '''     
        The main function
    '''
    print("parse xml file...")
    parse(xmlFile)
    
    if printFlag:
        print("\n---printing---")
        printtree()

    if smtFlag:
        print("\nsmt...")
        smt()  

# ########################
# ######## MAIN ##########
# ########################
if __name__ == '__main__':
    
    # Set options
    parser = argparse.ArgumentParser(description=bcolors.HEADER+\
                                    'Symbolic execution for policy' +\
                                    bcolors.ENDC)
    parser.add_argument( '--smt',
                         type=int,
                         default=0,
                         help="smt flag, i.e., --smt 1")
    parser.add_argument( '--file',
                         default='sample.xml',
                         help="pass the xml file name\
                          , e.g. \
                          --file sample.xml")
    parser.add_argument('-p', action="store_true",\
                             default=False,\
                             help='print option to\
                              parse policy file')
    args = parser.parse_args()
    smtFlag = args.smt
    printFlag = args.p
    # must be in the same directory
    xmlFile = args.file

    name_of_policy_folder = "policyFiles"
    file_path = os.path.join(name_of_policy_folder,xmlFile)

    # Call the main function
    main(file_path)