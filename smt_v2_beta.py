#!/usr/bin/python

"""
Parse and check policy files
for safety check of an IoT system
"""
from z3 import *
import xml.etree.ElementTree as ET
import os
import hashlib
from inspect import currentframe, getframeinfo

# Set up option parsing for smt run
import argparse

cf = currentframe()
filename = getframeinfo(cf).filename

# list of attributes and elements for each policy
# is saved in to the list
rulelist=[]
ruleatt=[]
dtype=[]
vendor=[]
daysAvailableInWeek=[]
timeatr=[]
user=[]
group=[]

class bcolors:
    '''
        notifciation colors
    '''
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def parse(xmlFile):
    """
        Parse with element tree library
    """
    tree = ET.parse(xmlFile)
    root = tree.getroot()

    for rule in root.iter('rule'):
        ruleatt.append(rule.attrib)
        rulelist.append(rule.text)

    for dtype2 in root.iter('type'):
        dtype.append(dtype2.text)

    for vendor2 in root.iter('vendor'):
        vendor.append(vendor2.text)

    for time2 in root.iter('time'):
        timeatr.append(time2.attrib)
        daysAvailableInWeek.append(time2.text)

    for user2 in root.iter('user'):
        user.append(user2.text)

    for group2 in root.iter('group'):
        group.append(group2.text)

def printtree():
    '''
        print for demo
    '''
    print ("rulelist=")
    pp(rulelist)
    print("-----------------")
    print ("ruleatt=")
    pp(ruleatt)
    print("-----------------")
    print ("dtype=")
    pp(dtype)
    print("-----------------")
    print ("vendor=")
    pp(vendor)
    print("-----------------")
    print ("daysAvailableInWeek=")
    pp(daysAvailableInWeek)
    print("-----------------")
    print ("timeatr=")
    pp(timeatr)
    print("-----------------")
    print ("user=")
    pp(user)
    print("-----------------")
    print ("group=")
    pp(group)
    print("-----------------")

def smt():
    '''
        The SMT solver function using Z3
    '''

    ###############################
    ### Helper functions secton ###
    ###############################
    def getRoomId(Name):
        """
            Helper inner function
            return and ID integer value
            for now just a number 
        """
        if Name.lower() == "room1temperature":
            return 10
        elif Name.lower() == "room1humidity":
            return 11
        if Name.lower() == "room2temperature":
            return 20
        elif Name.lower() == "room2humidity":
            return 21
        if Name.lower() == "room3temperature":
            return 30
        elif Name.lower() == "room3humidity":
            return 31
        if Name.lower() == "hallwaytemperature":
            return 40
        elif Name.lower() == "hallwayhumidity":
            return 41
        elif Name.lower() == "gcs":
            return 5
        elif Name.lower() == "airport":
            return 6
        elif Name.lower() == "terminal1":
            return 7
        elif Name.lower() == "drone1":
            return 8
        else:
            return None

    def daysXint(stringDay):
        ''' 
            Helper inner function
            pass a string and return a 
            set of integers earch representing a day
        ''' 
        daySet = set()
        if "Mon" in stringDay:
            daySet.add(1)

        if "Tue" in stringDay:
            daySet.add(2)

        if "Wed" in stringDay:
            daySet.add(3)

        if "Thu" in stringDay:
            daySet.add(4)

        if "Fri" in stringDay:
            daySet.add(5)

        if "Sat" in stringDay:
            daySet.add(6)

        if "Sun" in stringDay:
            daySet.add(7)

        return daySet

    def timeXseconds(time_str):
        '''
            Helper inner function 
            return the list for min max as seconds
            between zero and 24*3600 s
            TODO: use list comprehension in future
        ''' 
        def get_sec(time_str):
            h, m = time_str.split(':')
            return int(h) * 3600 + int(m) * 60

        return get_sec(time_str)

    def hashfunc(s):
        return int(hashlib.sha256\
            (s.encode('utf-8')).hexdigest(), 16) % 10**8

# ###########################################################
    # #################################################
    # ### Start iterating through the policy rules ####
    # #################################################
    
    # Symbolic execution: create a new Z3 solver
    solver = Solver() 

    # symbolic time
    _symb_time = Real('timeOfDay')
    solver.add(_symb_time>=0)
    solver.add(_symb_time<=24*3600) # time of the day in seconds
    
    _symb_dayOfWeek = Int('dayOfWeek')
    solver.add(_symb_dayOfWeek>=1) # 1 stands for Monday
    solver.add(_symb_dayOfWeek<=7) # 7 stands for Sunday

    # >>>>>>>> TODO: it could be humidity? then what? <<<<
    _symb_temp = Real('temperature')
    solver.add(_symb_temp>=-20)
    solver.add(_symb_temp<=80)

    _symb_humidity = Real('humidity')
    solver.add(_symb_humidity>=0)
    solver.add(_symb_humidity<=100)
    
    _symb_vendor=Int('vendor')
    _symb_user = Int('user')

    isThereConflict = False    
    conflictRules=[]
    conflicts=0

    for i in range(len(rulelist)):
        
        isThereConflict = _symb_vendor==getRoomId(vendor[i]+dtype[i])
        
        isThereConflict = isThereConflict and \
                          _symb_user == hashfunc(user[i])

        print "1- isThereConflict: " + str(isThereConflict)
        
        # Variable range
        # if dtype[i].lower() == "temperature":
        #     isThereConflict = isThereConflict and \
        #             _symb_temp >= ruleatt[i]['min'] and \
        #             _symb_temp <= ruleatt[i]['max']

        # elif dtype[i].lower() == "humidity":
        #     isThereConflict = isThereConflict and \
        #             _symb_humidity >= ruleatt[i]['min'] and \
        #             _symb_humidity <= ruleatt[i]['max']

        print "line " + str(cf.f_lineno) + ": " + str(isThereConflict)

        # Day of the week in a set
        dayIsAcceptable = False
        iterateSet = daysXint(daysAvailableInWeek[i])
        for d in iterateSet:
            dayIsAcceptable = dayIsAcceptable or \
                              (d == _symb_dayOfWeek)

        print "dayIsAcceptable= " + str(dayIsAcceptable)

        # Time range
        isThereConflict = isThereConflict and dayIsAcceptable and\
         _symb_time >= timeXseconds(timeatr[i]['min']) and \
         _symb_time <= timeXseconds(timeatr[i]['max'])

        conflictRules.append(isThereConflict)

        print "conflictRules:" 
        print conflictRules

        conflicts= If( isThereConflict,
                       conflicts+1,
                       conflicts)
    
    solver.add(conflicts > 1)

    # if isThereConflict is not flase then this is sat
    # if it is sat it is bad! it means there are conflicts
    if solver.check() == sat:
        print bcolors.FAIL+bcolors.UNDERLINE+\
              u'\u274c'+" found a safety issue"+\
              bcolors.ENDC

        model = solver.model()
        # print("temperature is "+ str(model.evaluate(_symb_temp)))
        # print("time is "+ str(model.evaluate(_symb_time)))
        # print("room is "+ str(model.evaluate(_symb_vendor)))
        # for d in model.decls():
        #     print "%s = %s" % (d.name(),model[d])

        for i,policy in enumerate(conflictRules):
            if model.evaluate(policy):
                print "one of the conflicits is in rule "+str(i)


    else:
        print bcolors.OKGREEN+\
              u'\u2714'+" all policies are good"+\
              bcolors.ENDC

def main(xmlFile):
    '''     
        The main function
    '''
    print("parse xml file...")
    parse(xmlFile)
    
    if printFlag:
        print("\n---printing---")
        printtree()

    if smtFlag:
        print("\nsmt...")
        smt()  

# ########################
# ######## MAIN ##########
# ########################
if __name__ == '__main__':
    
    # Set options
    parser = argparse.ArgumentParser(description=bcolors.HEADER+\
                                    'Symbolic execution for policy' +\
                                    bcolors.ENDC)
    parser.add_argument( '--smt',
                         type=int,
                         default=0,
                         help="smt flag, i.e., --smt 1")
    parser.add_argument( '--file',
                         default='sample.xml',
                         help="pass the xml file name\
                          , e.g. \
                          --file sample.xml")
    parser.add_argument('-p', action="store_true",\
                             default=False,\
                             help='print option to\
                              parse policy file')
    args = parser.parse_args()
    smtFlag = args.smt
    printFlag = args.p
    # must be in the same directory
    xmlFile = args.file

    name_of_policy_folder = "policyFiles"
    file_path = os.path.join(name_of_policy_folder,xmlFile)

    # Call the main function
    main(file_path)