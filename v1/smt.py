#!/usr/bin/python

"""Parse and check policy files for safety of an IoT system
"""

from z3 import *
import xml.etree.ElementTree as ET

rulelist=[]
ruleatt=[]
dtype=[]
vendor=[]
tttime=[]
timeatr=[]
user=[]
group=[]


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def parse():
    """
        Parse w/ element tree library
    """
    tree = ET.parse('sample.xml')
    root = tree.getroot()

    for rule in root.iter('rule'):
        ruleatt.append(rule.attrib)
        rulelist.append(rule.text)

    for dtype2 in root.iter('type'):
        dtype.append(dtype2.text)

    for vendor2 in root.iter('vendor'):
        vendor.append(vendor2.text)

    for time2 in root.iter('time'):
        timeatr.append(time2.attrib)
        tttime.append(time2.text)

    for user2 in root.iter('user'):
        user.append(user2.text)

    for group2 in root.iter('group'):
        group.append(group2.text)

def printtree():
    '''
        print for demo
    '''
    print rulelist
    print("-----------------")
    print ruleatt
    print("-----------------")
    print dtype
    print("-----------------")
    print vendor
    print("-----------------")
    print tttime
    print("-----------------")
    print timeatr
    print("-----------------")
    print user
    print("-----------------")
    print group
    print("-----------------")

def smt():
    '''
        The symbolic solver for policies
    '''
   #create a new solver
   solver = Solver()

    # symbolic time
    _time1_ = Real('Time')
    solver.add(_time1_>=0)
    solver.add(_time1_<=24*3600)
    
    temperature = Real('Temperature')
    solver.add(temperature>=-5)
    solver.add(temperature<=100)
    satisfiedPolicies=0

    #Check if there are vendors of a same?
    for i in range(len(rulelist)):
        for j in range(i+1,len(rulelist)):
            if vendor[i] == vendor[j] and \
             dtype[i] == dtype[j]:
                #print for the sake of have it shown
                print( bcolors.UNDERLINE + "found in:"+vendor[i])
                print(bcolors.ENDC+\
                    bcolors.WARNING+"user1: "+user[i]+"\n"\
                    "user2: "+user[j]+bcolors.ENDC)
                policyIsSat=True


    # for policy in policies:
    #     if policy.room == "Room1" and \
    #        policy.type=="Temperature":
    #        policyIsSat=((_time1_>=policy.minTime) and \
    #         (_time1_<=policy.maxTime) \
    #         (temp>=policy.maxValue) and \
    #         (temp<=policy.minValue))

    satisfiedPolicies=If(policyIsSat,
                          satisfiedPolicies+1,
                          satisfiedPolicies)
    solver.add(satisfiedPolicies>1)

    if solver.check() == unsat:
        print bcolors.FAIL+bcolors.UNDERLINE+\
              "found a safety issue"+\
              bcolors.ENDC
    else:
        print "all policies are good"

def main():
    '''
        The main function
    '''
    print("parse xml file...")
    parse()
    print("\n---printing---")
    printtree()

    print("\nsmt...")
    smt()   


if __name__ == '__main__':
    main()