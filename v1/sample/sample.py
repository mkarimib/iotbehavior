#!/usr/bin/python
from z3 import *
import xml.etree.ElementTree as ET

def parse():
    tree = ET.parse('sample.xml')
    root = tree.getroot()
    # root_tag = root.tag
    # print(root_tag) 

    # for form in root.findall("./policy/rule"):
    #     x=(form.attrib)
    #     z=list(x)
    #     for i in z:
    #         print(x[i])

    # for child in root:
    #     print(child.tag, child.attrib)
    #     print(child.child.tag, child.child.attrib)

    # print [elem.tag for elem in root.iter()]

    for rule in root.iter('rule'):
        print(rule.attrib)
        print(rule.text)

def smt():
    x = Real('x')
    y = Real('y')
    z = Real('z')
    s = Solver()
    s.add(3*x + 2*y - z == 1)
    s.add(2*x - 2*y + 4*z == -2)
    s.add(-x + 0.5*y - z == 0)
    print s.check()
    print s.model()

def main():
    # print("smt first")
    smt()
    print("parse xml file")
    parse()

if __name__ == '__main__':
    main()
