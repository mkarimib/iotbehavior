#!/usr/bin/python

"""try out some examples in z3 solver 
theorem prover, symbolic execution
"""

from z3 import *


def example1():
    x = Int('x')
    y = Int('y')
    s = solve(x > 2, y < 10, x + 2*y == 7)

def example2():
    x = Int('x')
    y = Int('y')
    print simplify(x + y + 2*x + 3)
    print simplify(x < y + x + 2)
    print simplify(And(x + 1 >= 3, x**2 + x**2 + y**2 + 2 >= 5))

def example3():
    p = Bool('p')
    q = Bool('q')
    r = Bool('r')
    s = Solver()
    print s
    s.add(Implies(p, q), r == Not(q), Or(Not(p), r))
    print s.check()

def example4():
    x = Real('x')
    s = Solver()
    s.add(2**x == 3)
    print s.check()

def example5():
    x = Real('x')
    y = Real('y')
    s = Solver()
    s.add(x > 1, y > 1, Or(x + y > 3, x - y < 2))
    print "asserted constraints..."
    for c in s.assertions():
        print c
    print s.check()
    print "statistics for the last check method..."
    print s.statistics()
    # Traversing statistics
    for k, v in s.statistics():
        print "%s : %s" % (k, v)    

def example6():
    X = [ Int('x%s' % i) for i in range(5) ]
    Y = [ Int('y%s' % i) for i in range(5) ]
    print X
    # Create a list containing X[i]+Y[i]
    X_plus_Y = [ X[i] + Y[i] for i in range(5) ]
    print X_plus_Y

    # Create a list containing X[i] > Y[i]
    X_gt_Y = [ X[i] > Y[i] for i in range(5) ]
    print X_gt_Y

    print And(X_gt_Y)

    # Create a 3x3 "matrix" (list of lists) of integer variables
    X = [ [ Int("x_%s_%s" % (i+1, j+1)) for j in range(3) ]
          for i in range(3) ]
    pp(X)

def example7():
    X = IntVector('x', 5)
    Y = RealVector('y', 5)
    P = BoolVector('p', 5)
    print X
    print Y
    print P
    print [ y**2 for y in Y ]
    print Sum([ y**2 for y in Y ])


def Kinematic():
    d, a, t, v_i, v_f = Reals('d a t v__i v__f')

    equations = [
       d == v_i * t + (a*t**2)/2,
       v_f == v_i + a*t,
    ]
    print "Kinematic equations:"
    print equations

    # Given v_i, v_f and a, find d
    problem = [
        v_i == 30,
        v_f == 0,
        a   == -8
    ]
    print "Problem:"
    print problem

    print "Solution:"
    solve(equations + problem)

def Sudoku():
    # 9x9 matrix of integer variables
    X = [ [ Int("x_%s_%s" % (i+1, j+1)) for j in range(9) ]
          for i in range(9) ]

    # each cell contains a value in {1, ..., 9}
    cells_c  = [ And(1 <= X[i][j], X[i][j] <= 9)
                 for i in range(9) for j in range(9) ]

    # each row contains a digit at most once
    rows_c   = [ Distinct(X[i]) for i in range(9) ]

    # each column contains a digit at most once
    cols_c   = [ Distinct([ X[i][j] for i in range(9) ])
                 for j in range(9) ]

    # each 3x3 square contains a digit at most once
    sq_c     = [ Distinct([ X[3*i0 + i][3*j0 + j]
                            for i in range(3) for j in range(3) ])
                 for i0 in range(3) for j0 in range(3) ]

    sudoku_c = cells_c + rows_c + cols_c + sq_c

    # sudoku instance, we use '0' for empty cells
    instance = ((0,0,0,0,9,4,0,3,0),
                (0,0,0,5,1,0,0,0,7),
                (0,8,9,0,0,0,0,4,0),
                (0,0,0,0,0,0,2,0,8),
                (0,6,0,2,0,1,0,5,0),
                (1,0,2,0,0,0,0,0,0),
                (0,7,0,0,0,0,5,2,0),
                (9,0,0,0,6,5,0,0,0),
                (0,4,0,9,7,0,0,0,0))

    instance_c = [ If(instance[i][j] == 0,
                      True,
                      X[i][j] == instance[i][j])
                   for i in range(9) for j in range(9) ]

    s = Solver()
    s.add(sudoku_c + instance_c)
    if s.check() == sat:
        m = s.model()
        r = [ [ m.evaluate(X[i][j]) for j in range(9) ]
              for i in range(9) ]
        print_matrix(r)
    else:
        print "failed to solve"

def main():
    '''
        The main function
    '''
    # example7()
    # Kinematic()
    Sudoku()    


if __name__ == '__main__':
    main()