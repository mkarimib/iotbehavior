#!/usr/bin/python
from z3 import *
import xml.etree.ElementTree as ET

rulelist=[]
ruleatt=[]
dtype=[]
vendor=[]
tttime=[]
timeatr=[]
user=[]
group=[]

def parse():
    tree = ET.parse('sample.xml')
    root = tree.getroot()

    for rule in root.iter('rule'):
        ruleatt.append(rule.attrib)
        rulelist.append(rule.text)

    for dtype2 in root.iter('type'):
        dtype.append(dtype2.text)

    for vendor2 in root.iter('vendor'):
        vendor.append(vendor2.text)

    for time2 in root.iter('time'):
        timeatr.append(time2.attrib)
        tttime.append(time2.text)

    for user2 in root.iter('user'):
        user.append(user2.text)

    for group2 in root.iter('group'):
        group.append(group2.text)

def printtree():
    print rulelist
    print("-----------------")
    print ruleatt
    print("-----------------")
    print dtype
    print("-----------------")
    print vendor
    print("-----------------")
    print tttime
    print("-----------------")
    print timeatr
    print("-----------------")
    print user
    print("-----------------")
    print group
    print("-----------------")

def smt():
    solver = Solver() #create a new solver

    time = Real('Time')
    # date = int('Days') # RTC
    # solver.add(date>=1745004224)
    # solver.add(date<=3291800099)
    solver.add(time>=0)
    solver.add(time<=24*3600)
    temperature = Real('Temperature')
    solver.add(temperature>=-5)
    solver.add(temperature<=100)
    satisfiedPolicies=[]

    for policy in policies:
        if policy.room == "Room1" and \
           policy.type=="Temperature":
           policyIsSat=((time>=policy.minTime) and \
            (time<=policy.maxTime) \
            (temp>=policy.maxValue) and \
            (temp<=policy.minValue))

    satisfiedPolicies=ITE(policyIsSat,
                          satisfiedPolicies+1,
                          satisfiedPolicies)
    solver.add(satisfiedPolicies>1)
    solver.check()

def smt2():
    solver = Solver() #create a new solver

    time = Real('Time')
    # date = int('Days') # RTC
    # solver.add(date>=1745004224)
    # solver.add(date<=3291800099)
    solver.add(time>=0)
    solver.add(time<=24*3600)
    temperature = Real('Temperature')
    solver.add(temperature>=-5)
    solver.add(temperature<=100)
    satisfiedPolicies=[]

    for policy in policies:
        if policy.room == "Room1" and \
           policy.type=="Temperature":
           policyIsSat = True
           for condition,variable in policy.requirements:
                policyIsSat= policyIsSat and \
                             condition(variable)

    satisfiedPolicies=ITE(policyIsSat,
                          satisfiedPolicies+1,
                          satisfiedPolicies)
    solver.add(satisfiedPolicies>1)
    solver.check()
    # print s.check()
    # print s.model()

def smt():
    # x = Real('x')
    # y = Real('y')
    # z = Real('z')
    solver = Solver() #create a new solver
    # s.add(3*x + 2*y - z == 1)
    # s.add(2*x - 2*y + 4*z == -2)
    # s.add(-x + 0.5*y - z >= 1000)
    
    time = Real('Time')
    date = int('Days') # RTC
    solver.add(date>=1745004224)
    solver.add(date<=3291800099)
    solver.add(time>=0)
    solver.add(time<=24*3600)
    temperature = Real('Temperature')
    solver.add(temperature>=-5)
    solver.add(temperature<=100)
    satisfiedPolicies=[]

    for policy in policies:
        if policy.room == "Room1" and \
           policy.type=="Temperature":
           policyIsSat=((time>=policy.minTime) and \
            (time<=policy.maxTime) \
            (temp>=policy.maxValue) and\
             (temp<=policy.minValue))

    satisfiedPolicies=ITE(policyIsSat,
                          satisfiedPolicies+1,
                          satisfiedPolicies)
    solver.add(satisfiedPolicies>1)
    solver.check()
    # print s.check()
    # print s.model()


def main():
    print("parse xml file...")
    parse()
    print("\n---printing---")
    printtree()

    # print("\nsmt...")
    # smt()   


if __name__ == '__main__':
    main()